const {createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            // Vetor de imagens locais
            imagensLocais:['./imagens/lua.jpg', './imagens/sol.jpg', './imagens/SENAI_logo.png'],

            imagensInternet:[
                'https://static.nationalgeographicbrasil.com/files/styles/image_3200/public/0101-cassini-gallery-crash.jpg?w=1900&h=1900',
                'https://thumbs.web.sapo.io/?W=800&H=0&delay_optim=1&epic=YWM4eOg/4zsEKP4yKydPayZxV80JwZTyLnZp/NL2nQ3WbZTCa5Y7hAysMI5zdOlElFHKuH649pRLjAD4bdPXbowe21OXNclPXFfpXxoyZX5AXaE=',
                'https://conteudo.imguol.com.br/c/noticias/04/2021/12/14/plutao-1639493431655_v2_1x1.jpg',
                'https://s1.static.brasilescola.uol.com.br/be/2021/11/planeta-marte.jpg'
            ]

        }; //Fim return
    }, //Fim data()

    computed:{
        randomImage(){
            return this.imagensLocais[this.randomIndex];
        }, //fechamento randomImage()
        randomImageNet(){
            return this.imagensInternet[this.randomIndexInternet];
        },
    }, //fechamento computed

    methods:{
        getRandomImg(){
            this.randomIndex = Math.floor(Math.random() * this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random() * this.imagensInternet.length);
        }//fechamento getRandomImg
    }, //fechaento methodes

}).mount("#app");