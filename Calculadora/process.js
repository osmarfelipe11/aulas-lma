const { createApp } = Vue;
createApp({
  data() {
    return {
      display: "0",
      operandoAtual: null,
      operandoAnterior: null,
      operador: null,
    }; //Fechamento do return
  }, //Fechamento "data()"
  methods: {
    handleButtonClick(botao) {
      switch (botao) {
        case '+':
        case '-':
        case '/':
        case '*':
            this.handleOperation(botao);
            break;


        case ".":
            this.handleDecimal();
            break;

        case "=":
            this.handleEquals();
            break;
        default:
          this.handleNumber(botao);
          break;
      } //Fechamento switch
    }, //Fechamento handleButtonClick
    
    handleNumber(numero) {
      if (this.display === "0") {
        this.display = numero.toString();
      } else {
        this.display += numero.toString(); //this.display = this.display + numero.toString()
      }
    }, //fechamento handleNumber
    handleOperation(operacao){
      if(this.operandoAtual !== null){
        this.handleEquals();
      }// Fechamento if
        this.operador = operacao;

        this.operandoAtual = parseFloat(this.display);
        this.display = "0";

    },//fechamento handleOperation

    handleEquals(){
      const displayAtual = parseFloat(this.display);
      if(this.operandoAtual !== null && this.operador !== null){
        switch(this.operador){
          case "+":
            this.display = (this.operandoAtual + displayAtual).toString();
            break;
          case "-":
            this.display = (this.operandoAtual - displayAtual).toString();
            break;
          case "*":
            this.display = (this.operandoAtual * displayAtual).toString();
            break;
          case "/":
            this.display = (this.operandoAtual / displayAtual).toString();
            break;
        }// Fechamento switch
        this.operandoAnterior = this.operandoAtual;
        this.operandoAtual = null;
        this.operador = null;
      }// Fechamento if
      else{
        this.operandoAnterior = displayAtual;
      }//Fechamento do else
    },//Fechamento handleEquals

    handleDecimal(){
      if(!this.display.includes(".")){
        this.display += ".";
      }//Fechamento if
    }, // Fechamento handleDecimal

    handleClear(){
      this.display = "0";
      this.operandoAnterior = null;
      this.operandoAnterior = null;
      this.operador = null;
    },//Fechamento clear
  }, //Fechamento methodes

}).mount("#app"); // Fechamento da função "createApp"
