//Processamento dos dados do formulário "index.html"

//Acessando os elementos formulário do html
const formulario = document.getElementById("formulario1");

//adicionar um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function(evento){

    evento.preventDefault(); //previne o comportamento padrão de um elemento HTML em resposta a um evento

    //variáveis para tratar os dados recebidos dos elementos formulário

    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //Exibe um alerta com os dados

   // alert(`Nome: ${nome} --- Email: ${email}`);

    const updateResultado = document.getElementById("resultado");
    updateResultado.value = `Nome: ${nome} --- Email: ${email}`;

    updateResultado.style.width = updateResultado.scrollWidth + "px";

});